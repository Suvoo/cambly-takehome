def main(text):
    # 1. preprocess data by removing punctuations
    punctutations = '''!()-[]{};:'"\,<>./?@#$%^&*_~''' # See Reference [1]
    # o(n)
    for ele in text:
        if ele in punctutations:
            text = text.replace(ele, " ")
    # 2. We are ignoring the Case when counting
    text = text.lower()
    # 3. Extra spaces are taken care of as words are then stored in array
    text = text.split()    

    freq = {}
    for s in text:
        if s not in freq:
            freq[s] = 1
        else:
            freq[s] += 1

    freq = dict(sorted(freq.items(), key=lambda x: (-x[1], x[0]))) # See Reference [2]

    for f in freq:
        print(f,freq[f])


if __name__ == '__main__':
    # text = "This is a test. That is not a test. Test"
    text = "From the moment the first immigrants arrived on these shores, generations of parents have worked hard and sacrificed whatever is necessary so that their children could have the same chances they had; or the chances they never had. Because while we could never ensure that our children would be rich or successful; while we could never be positive that they would do better than their parents, America is about making it possible to give them the chance. To give every child the opportunity to try. Education is still the foundation of this opportunity. And the most basic building block that holds that foundation together is still reading. At the dawn of the 21st century, in a world where knowledge truly is power and literacy is the skill that unlocks the gates of opportunity and success, we all have a responsibility as parents and librarians, educators and citizens, to instill in our children a love of reading so that we can give them the chance to fulfill their dreams."
    main(text)
    # output present in the txt file attached

# [1] https://www.geeksforgeeks.org/python-remove-punctuation-from-string/
# [2] https://stackoverflow.com/questions/44076269/sort-counter-by-frequency-then-alphabetically-in-python

